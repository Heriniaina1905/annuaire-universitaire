package org.annuaire.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Etudiant {
	private String nom;
	private String prenom;
	private String genre;
	private String date;
	private String lieux;
	private String etablissement;
	private String secteur;
}
