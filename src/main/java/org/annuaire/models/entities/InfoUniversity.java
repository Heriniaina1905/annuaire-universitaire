package org.annuaire.models.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class InfoUniversity {
//	private Date rentree_universitaire;
	private String date;
	private String lieux;
	private String etablissement;
	private String secteur;
}
