package org.annuaire.models.dao;

import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.Date;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExportExcel {
	
	static String firstCharToUpperCase(String string) {
    	String result = "";
    	char[] tableauDeChar = string.toCharArray();
    	String firstCharToUpperCase = String.valueOf(tableauDeChar[0]).toUpperCase();
    	result+=firstCharToUpperCase;
    	for(int index = 1; index<tableauDeChar.length; index++) {
    		result+=String.valueOf(tableauDeChar[index]);
    	}
    	return result;
    }
	public double Total(Object[] liste, String attribut)throws Exception{
		double total = 0;
		String maj = firstCharToUpperCase(attribut);
		Class my_class = liste[0].getClass();
		String get_field = "get".concat(maj);
		Method method = my_class.getMethod(get_field,new Class[0]);
		for(int index=0;index<liste.length;index++){
			if (method.getReturnType().getName().contains("int")) {
				total += (Integer) method.invoke(liste[index] , new Object[0]); 
			} else if (method.getReturnType().getName().contains("double")) {
				total += (Double) method.invoke(liste[index],new Object[0]); 
			} else if (method.getReturnType().getName().contains("float")) {
				total += (Float) method.invoke(liste[index],new Object[0]); 
			}
		}
		return total;
    }
	
	public String turnToString(Object objet){
		String resultat="";
		if((objet.getClass().getName().equals("java.lang.Integer")) || (objet.getClass().getName().equals("java.lang.Double")) || (objet.getClass().getName().equals("java.lang.Float"))){
			resultat = resultat.valueOf(objet);  
		}
		else if(objet.getClass().getName().equals("java.sql.Date")){
			resultat="TO_DATE('"+objet+"','YYYY-MM-DD')";
		}
		else{
			resultat=(String)objet;
		}
		return resultat;
	}
	
	public Object[] getAllValues(Object objet,String[] attributs)throws Exception{
		Object[] resultats;
		Class classe = objet.getClass();
		Class[] empty = new Class[0];
		Object[] objetTempo = new Object[0];
		Method[] methodes=new Method[attributs.length];
		resultats = new Object[attributs.length];
		for(int index=0; index<attributs.length; index++){
			String maj = firstCharToUpperCase(attributs[index]);
			String meth="get".concat(maj);
			methodes[index] = classe.getMethod(meth,empty);
		}
		for(int index=0;index<methodes.length;index++){
			resultats[index]=new Object();
			resultats[index]=methodes[index].invoke(objet,objetTempo);
		}
		return resultats;
	}
	
	public static void exportXLS(String path, String fileName,String title,String[][] specialInfo,Object[] list,String[] fields)throws Exception{
		HSSFWorkbook work = new HSSFWorkbook();
		HSSFSheet sheet = work.createSheet(fileName);
		int row = 0;
		HSSFRow rowtitle = sheet.createRow(row);
		rowtitle.createCell(0).setCellValue(title);

		
		row++;
		if(specialInfo == null){
			
		}else{
			for(int index = 0; index<specialInfo.length; index++) {
				HSSFRow rowinfo = sheet.createRow(row);
				for(int i = 0; i<specialInfo[index].length; i++) {
					rowinfo.createCell(i).setCellValue(specialInfo[index][i]);
				}
				row++;
			}	
		}
		
		
		HSSFRow rowhead = sheet.createRow(row);
		for(int indice = 0; indice<fields.length; indice++){
			rowhead.createCell(indice).setCellValue(fields[indice].toUpperCase());
		}
		
		String get_field = " ", maj = " ";
		Class my_class = list[0].getClass();
		
		Method method = null;
		
		for(int indexList = 0; indexList<list.length; indexList++){
			row = row+1;
			HSSFRow ligne = sheet.createRow(row);
			for(int indexField = 0; indexField<fields.length; indexField++){
				maj = firstCharToUpperCase(fields[indexField]);
				get_field = "get".concat(maj);
				method = my_class.getMethod(get_field,new Class[0]);
				if (method.getReturnType().getName().contains("String")) {
					ligne.createCell(indexField).setCellValue((String)method.invoke(list[indexList], new Object[0]));
				} else if (method.getReturnType().getName().contains("Number")) {
					ligne.createCell(indexField).setCellValue(String.valueOf((Number)method.invoke(list[indexList], new Object[0])));
				} else if (method.getReturnType().getName().contains("int")) {
					ligne.createCell(indexField).setCellValue(String.valueOf((Integer)method.invoke(list[indexList], new Object[0])));
				} else if (method.getReturnType().getName().contains("double")) {
					ligne.createCell(indexField).setCellValue(String.valueOf((Double)method.invoke(list[indexList], new Object[0])));
				} else if (method.getReturnType().getName().contains("float")) {
					ligne.createCell(indexField).setCellValue(String.valueOf((Float)method.invoke(list[indexList], new Object[0])));
				} else if (method.getReturnType().getName().contains("Date")) {
					ligne.createCell(indexField).setCellValue(String.valueOf((Date)method.invoke(list[indexList], new Object[0])));
				} 
			}
		}
		
		String out = path;
		FileOutputStream fileOPS = new FileOutputStream(out);
		work.write(fileOPS);
		fileOPS.close();
		System.out.println(fileName+" exported successfully");
	}
}
