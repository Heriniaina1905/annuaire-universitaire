package org.annuaire.models.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Set;
import java.util.TreeMap;

import org.annuaire.models.entities.Etudiant;

public class InputOutput {
	
	static int key = 0;
	PrintWriter mpanoratra;
//	private static final Charset cp850Charset = Charset.forName("windows-1252");
//	private static final Charset utf8Charset = Charset.forName("UTF-8");
	
	public TreeMap<String, Etudiant> importTxt(File fichier) throws IOException{
		TreeMap<String, Etudiant> result = new TreeMap<String, Etudiant>();
		int counter = 0;
		FileReader fr = null;
		BufferedReader br = null;
		try{
			fr = new FileReader(fichier);
			br = new BufferedReader(fr);
			String line = "";
			while(line != null){
				line = br.readLine();
				
				byte[] byteLine = line.getBytes();
				line = new String(byteLine, "UTF-8");
				
				String[] arrayString = line.split("\t");
				
//				InfoUniversity info = new InfoUniversity(arrayString[0], arrayString[1], arrayString[2], arrayString[3]);
				Etudiant etudiant = new Etudiant(arrayString[5], arrayString[6], arrayString[4], arrayString[0], arrayString[1], arrayString[2], arrayString[3]);
				
				result.put(generateKey(arrayString[5]), etudiant);
				counter += 1;
			}
		} catch(Exception e){
			if(counter >= 3000 - 1){
				fr.close();
				br.close();
				return result;
			}
			e.printStackTrace();
			int ind = counter+1;
			System.out.println("Erreur  : "+e.getMessage()+" Au "+ind+"�me �lements");
		} finally{
			fr.close();
			br.close();
		}
		return result;
	}
	
	public boolean ajouterEtudiant(TreeMap<String, Etudiant> treemap, Etudiant etudiant){
		try{
			treemap.put(generateKey(etudiant.getNom()), etudiant);
			return true;
		}catch(Exception e){
			
		}
		
		return false;
	}
	
	public String exportTxt(TreeMap<String, Etudiant> treemap, String path){
		try{
			
			File fichier = new File(path);
			mpanoratra = new PrintWriter(fichier, "UTF-8");
			Set<String> keys = treemap.keySet();
			
			for(String key : keys){
				System.out.println("Obtensio de l'�l�ment dans le treemap");
				Etudiant etudiant = treemap.get(key);
				
				mpanoratra.println(etudiant.getDate()
								+"\t"
								+etudiant.getLieux()
								+"\t"
								+etudiant.getEtablissement()
								+"\t"
								+etudiant.getSecteur()
								+"\t"
								+etudiant.getGenre()
								+"\t"
								+etudiant.getNom()
								+"\t"
								+etudiant.getPrenom()
						);
			}
			mpanoratra.close();
			return "Fichier export� avec succ�s!";
		}catch(Exception e){
			e.printStackTrace();
		}
		return "L'exportation a �chou�!";
	}
	
	public String getAllEtablissement(){
		return null; 
	}
	
	public static String generateKey(String nom){
		key += 1;
		return nom+key;
	}
}
