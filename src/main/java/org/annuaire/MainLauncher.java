package org.annuaire;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.sun.javafx.scene.control.GlobalMenuAdapter;

import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import net.sf.image4j.codec.ico.ICODecoder;

public class MainLauncher extends Application {

	@Override
	public void start(Stage primaryStage) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource("views/MainView.fxml"));
		
	    Scene scene = new Scene(root, 1920, 1000);
	    
	    scene.getStylesheets().add("org/annuaire/views/css/MainStyle.css");
	    
	    ArrayList<Image> lImages = new ArrayList<>();
	    ICODecoder.read(new File("src/main/resources/icone.ico")).stream().forEach((lBufferedImage) -> lImages.add(SwingFXUtils.toFXImage(lBufferedImage, null)));
	    primaryStage.getIcons().addAll(lImages);
	    //	    primaryStage.getIcons().add(new Image("file:src/main/resources/icone.ico"));
	    primaryStage.setTitle("Annuaire Universitaire");
	    primaryStage.setScene(scene);
	    primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
