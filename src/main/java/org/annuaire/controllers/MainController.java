package org.annuaire.controllers;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;

import org.annuaire.models.dao.ExportExcel;
import org.annuaire.models.dao.InputOutput;
import org.annuaire.models.entities.Etudiant;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class MainController {
	
    @FXML
    private TableView<Etudiant> listeEtudiantTableView;

    @FXML
    private TableColumn<Etudiant, String> nomColone;

    @FXML
    private TableColumn<Etudiant, String> prenomColone;

    @FXML
    private TableColumn<Etudiant, String> genreColone;

    @FXML
    private TableColumn<Etudiant, String> secteurColone;

    @FXML
    private TableColumn<Etudiant, String> etablissementColone;

    @FXML
    private TableColumn<Etudiant, String> lieuxColone;

    @FXML
    private TableColumn<Etudiant, String> dateColone;

    @FXML
    private TextField nomModifTextField;

    @FXML
    private TextField prenomModifTextField;

    @FXML
    private ComboBox<String> genreModifComboBox;

    @FXML
    private TextField lieuxModifTextField;

    @FXML
    private TextField etablissementModifTextField;

    @FXML
    private TextField secteurModifTextField;

    @FXML
    private ComboBox<String> secteurSearch;
    
    @FXML
    private ComboBox<String> dateCombo;

    @FXML
    private Label msgModif;

    @FXML
    private Button modifierButton;

    @FXML
    private Button ajouterButton;

    @FXML
    private Button annuleButton;

    @FXML
    private Button annuleModif;
    
    @FXML
    private TextField nomAjouterTextField;

    @FXML
    private TextField prenomAjouterTextField;

    @FXML
    private ComboBox<String> genreAjouterComboBox;

    @FXML
    private TextField lieuxAjouterTextField;

    @FXML
    private TextField etablissementAjouterTextField;

    @FXML
    private ComboBox<String> secteurAjouterComboBox;

    @FXML
    private ComboBox<String> dateAjouterCombo;

    @FXML
    private Label msgAjouter;

    @FXML
    private Button supprimerButton;

    @FXML
    private TextField nomSearchTextField;

    @FXML
    private TextField prenomSearchTextField;

    @FXML
    private ComboBox<String> genreSearchComboBox;

    @FXML
    private ComboBox<String> dateSearchCombo;

    @FXML
    private Button searchAllButton;

    @FXML
    private MenuItem importTextMenu;

    @FXML
    private MenuItem importExcelMenu;

    @FXML
    private MenuItem importCsvMenu;

    @FXML
    private MenuItem saveMenu;
    
    @FXML
    private MenuItem exportTextMenu;

    @FXML
    private MenuItem exportExcelMenu;

    @FXML
    private MenuItem exportCsvMenu;

    @FXML
    private MenuItem leaveMenu;

    @FXML
    private TextField searchTextField;

    @FXML
    private Button searchOneButton;
    
    @FXML
    private Label msg;
    
    @FXML
    private TextField universitySearchTextField;

    @FXML
    private TextField lieuxSearchTextField;
    
    InputOutput io = new InputOutput();
	
	TreeMap<String, Etudiant> listeEtudiants = new TreeMap<String, Etudiant>();
	
	ObservableList<Etudiant> listeEtudiant = null;
	
	ObservableList<String> secteursList = FXCollections.observableArrayList("�tablissements publics", "�tablissements priv�s");
	
	ObservableList<String> genreList = FXCollections.observableArrayList("Masculin", "Feminin");

	File data = new File("src/main/resources/data.txt");
	
    @FXML
    boolean crud(ActionEvent event) {
    	try{
    		cleanAllMessages();
    		if(event.getSource().equals(annuleButton))
    			throw new AnnuleException("Op�ration annul�");
    		
    		if(event.getSource().equals(modifierButton))
    			throw new ModifierException("Etudiant modifi�");
    		
    		if(event.getSource().equals(supprimerButton))
    			throw new SupprimeException("Etudiant Supprim�");
    		
    		if(event.getSource().equals(annuleModif))
    			throw new AnnuleException("Modification annul�");
    		
    		Etudiant e = new Etudiant(nomAjouterTextField.getText(), prenomAjouterTextField.getText(), genreAjouterComboBox.getValue(), dateAjouterCombo.getValue(), lieuxAjouterTextField.getText(), etablissementAjouterTextField.getText(), secteurAjouterComboBox.getValue());
    		io.ajouterEtudiant(listeEtudiants, e);
    		refreshTableView();
    		clear();
    		return true;
    	} catch(AnnuleException ane){
    		cleanAllMessages();
    		showModifMenu();
    	} catch (ModifierException e) {
			Etudiant etudiant = new Etudiant(nomModifTextField.getText(), prenomModifTextField.getText(), genreModifComboBox.getSelectionModel().getSelectedItem(), dateCombo.getSelectionModel().getSelectedItem().toString(), lieuxModifTextField.getText(), etablissementModifTextField.getText(), secteurModifTextField.getText());
			Etudiant cible = listeEtudiantTableView.getSelectionModel().getSelectedItem();
			
			deleteEtudiant(cible, "");
			io.ajouterEtudiant(listeEtudiants, etudiant);
			
			refreshTableView();
			
		} catch (SupprimeException e) {
			cleanAllMessages();
			Alert confirmation = new Alert(AlertType.CONFIRMATION);
			
			confirmation.setHeaderText("Demande de confirmation");
			confirmation.setContentText("Voulez-vous vraiment le supprimer?");
			
			Optional<ButtonType> reponse = confirmation.showAndWait();
			
			if(reponse.get().equals(ButtonType.CANCEL))
				return true;
			
			Etudiant reference = listeEtudiantTableView.getSelectionModel().getSelectedItem();
			
			deleteEtudiant(reference, e.getMessage());
		}
    	return true;
    }

    @FXML
    boolean exportFile(ActionEvent event) throws Exception {
    	try{
        	if(event.getSource().equals(saveMenu))
        		throw new SaveException("Donn�e sauvegard� avec succ�s!");
        	
        	if(event.getSource().equals(exportExcelMenu))
        		throw new ExportExcelException("Donn�e sauvegard� avec succ�s!");
        	if(event.getSource().equals(exportCsvMenu))
        		throw new CSVException("Donn�e sauvegard� avec succ�s!");
    	}catch(SaveException se){
    		io.exportTxt(listeEtudiants, data.getPath());
    		initialize();
    	}catch(ExportExcelException eee){
    		FileChooser fileChooser = new FileChooser();
        	
        	fileChooser.getExtensionFilters().addAll(
        			new FileChooser.ExtensionFilter("Excel Format", "*.xls"),
        			new FileChooser.ExtensionFilter("LibreOffice Calc", "*.ods")
        	);
        	
        	File selectFile = fileChooser.showSaveDialog(new Stage());
        	
        	Path chemin = selectFile.toPath();
        	
        	String[] fields = {
        			"nom",
        			"prenom",
        			"genre",
        			"date",
        			"lieux",
        			"etablissement",
        			"secteur",
        	};
        	
        	ExportExcel.exportXLS(chemin.toString(),chemin.getFileName().toString(), chemin.getFileName().toString(), null, listeEtudiants.values().toArray(), fields);
        	Alert message = new Alert(AlertType.INFORMATION);
        	message.setContentText(eee.getMessage());
        	message.show();
    	}catch(CSVException csve){
    		
    		FileChooser fileChooser = new FileChooser();
        	
        	fileChooser.getExtensionFilters().add(
        			new FileChooser.ExtensionFilter("CSV File", "*.csv")
        	);
        	
        	File selectFile = fileChooser.showSaveDialog(new Stage());
        	PrintWriter pw = new PrintWriter(selectFile, "UTF-8");
        	Set<String> keys = listeEtudiants.keySet();
    		for(String key : keys){
    			Etudiant e = listeEtudiants.get(key);
    			pw.write(e.getNom()
    					+","
    					+e.getPrenom()
    					+","
    					+e.getGenre()
    					+","
    					+e.getEtablissement()
    					+","
    					+e.getLieux()
    					+","
    					+e.getSecteur()
    					+","
    					+e.getDate()
    					+"\n"
    					);
    		}
    	}
    	return true;
    }

    @FXML
    boolean importFile(ActionEvent event) {
    	return true;
    }
    
    @FXML
    boolean leave(ActionEvent event){
    	Alert ask = new Alert(AlertType.NONE);
    	
    	ask.getButtonTypes().add(ButtonType.YES);
    	ask.getButtonTypes().add(ButtonType.NO);
    	ask.getButtonTypes().add(ButtonType.CANCEL);
    	
    	ask.setContentText("Vos donn�es modifi� peuvent �tre perdue sans sauvegarde, Voulez-vous sauvegarder?");
    	
    	Optional<ButtonType> reponse = ask.showAndWait();
    	
    	if(reponse.get().equals(ButtonType.YES))
    		io.exportTxt(listeEtudiants, data.getPath());
    	
    	if(reponse.get().equals(ButtonType.CANCEL))
    		return true;
    	
    	Stage stage = (Stage) supprimerButton.getScene().getWindow();
    	stage.close();
    	return true;
    }
    
    @FXML
    boolean getDesc(MouseEvent event) {
    	showModifMenu();
    	return true;
    }
	
	@FXML
    void initialize(){
		try{
			refreshTab();
			addAllComboBoxValues();
			showModifMenu();
		}catch(IOException io){
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@FXML
    boolean searchAll(KeyEvent event) {		
		String nom = nomSearchTextField.getText();
		String prenom = prenomSearchTextField.getText();
		String genre = genreSearchComboBox.getValue();
		String etablissement = universitySearchTextField.getText();
		String lieux = lieuxSearchTextField.getText();
		String date = dateSearchCombo.getSelectionModel().getSelectedItem();
		String secteur = secteurSearch.getValue();
		
		if(date == null)
			date = "";
		if(secteur == null)
			secteur = "";
		Etudiant etudiant = new Etudiant(nom, prenom, genre, date, lieux, etablissement, secteur);
		
		TreeMap<String, Etudiant> result = search(etudiant);
		
		listeEtudiant = FXCollections.observableArrayList(result.values());
		listeEtudiantTableView.getItems().removeAll(listeEtudiantTableView.getItems());
		
		listeEtudiantTableView.setItems(listeEtudiant);
		
		return true;
    }
	@FXML
    void handlerTableView(KeyEvent event) {
//		TODO
		try{
			if(event.getCode() == KeyCode.DELETE)
				throw new SupprimeException("Element supprim�");
		}catch(SupprimeException se){
			cleanAllMessages();
			Alert confirmation = new Alert(AlertType.CONFIRMATION);
			
			confirmation.setHeaderText("Demande de confirmation");
			confirmation.setContentText("Voulez-vous vraiment le supprimer?");
			
			Optional<ButtonType> reponse = confirmation.showAndWait();
			
			if(reponse.get().equals(ButtonType.CANCEL))
				return;
			
			Etudiant reference = listeEtudiantTableView.getSelectionModel().getSelectedItem();
			
			deleteEtudiant(reference, se.getMessage());
		}
    }
	
	void refreshTab() throws Exception{
		listeEtudiants = io.importTxt(data);
		
		refreshTableView();
    }
	
	void refreshTableView(){
		listeEtudiant = FXCollections.observableArrayList(listeEtudiants.values());
		
		listeEtudiantTableView.getItems().removeAll(listeEtudiantTableView.getItems());
		
		initCell();
		
		listeEtudiantTableView.setItems(listeEtudiant);
	}
    
    void initCell(){
		nomColone.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("nom"));
		prenomColone.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("prenom"));
		genreColone.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("genre"));
		etablissementColone.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("etablissement"));
		secteurColone.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("secteur"));
		lieuxColone.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("lieux"));
		dateColone.setCellValueFactory(new PropertyValueFactory<Etudiant, String>("date"));
    }
    
    boolean addAllComboBoxValues(){
    	genreAjouterComboBox.setItems(genreList);
    	genreAjouterComboBox.getSelectionModel().select(0);
    	
    	genreModifComboBox.setItems(genreList);
    	genreModifComboBox.getSelectionModel().select(0);
    	
    	genreSearchComboBox.setItems(genreList);
    	genreSearchComboBox.getSelectionModel().select(0);
    	
    	dateCombo.setItems(getAllDate());
    	dateSearchCombo.setItems(getAllDate());
    	
    	secteurSearch.setItems(secteursList);
    	secteurAjouterComboBox.setItems(secteursList);
    	
    	dateAjouterCombo.setItems(getAllDate());
    	dateAjouterCombo.getSelectionModel().select(0);
    	
    	return true;
    }
    
    boolean showModifMenu(){
    	try{		
    		Etudiant etudiant = listeEtudiantTableView.getSelectionModel().getSelectedItem();
    		
    		nomModifTextField.setText(etudiant.getNom());
    		prenomModifTextField.setText(etudiant.getPrenom());
    		genreModifComboBox.getSelectionModel().select(etudiant.getGenre());
    		etablissementModifTextField.setText(etudiant.getEtablissement());
    		secteurModifTextField.setText(etudiant.getSecteur());
    		lieuxModifTextField.setText(etudiant.getLieux());
    		dateCombo.setValue(etudiant.getDate());
    		return true;
    	}catch(Exception e){
    		e.getMessage();
    	}
    	return false;
    }
    
    boolean cleanAllMessages(){
    	msg.setText("");
    	msgAjouter.setText("");
    	msgModif.setText("");
    	return true;
    }
    
    private boolean deleteEtudiant(Etudiant reference, String message){
    	Set<String> keys = listeEtudiants.keySet();
		for(String key : keys){
			Etudiant etudiant= listeEtudiants.get(key);
			if(etudiant.equals(reference)){
				listeEtudiants.remove(key);
				System.out.println(etudiant.getNom()+" a �t� supprim� avec succ�s!");
				
				msg.setTextFill(Color.LIGHTGREEN);
				msg.setText(message);
				
				refreshTableView();
				break;
			}
		}
		return true;
    }
    
    TreeMap<String, Etudiant> search(Etudiant e){
    	TreeMap<String, Etudiant> result = new TreeMap<String, Etudiant>();
    	
    	Set<String> keys = listeEtudiants.keySet();
    	System.out.println(e.getNom());
    	for(String key : keys){
    		Etudiant parc = listeEtudiants.get(key);
    		if(parc.getNom().contains(e.getNom()) && parc.getPrenom().contains(e.getPrenom()) && parc.getGenre().contains(e.getGenre()) && parc.getEtablissement().contains(e.getEtablissement()) && parc.getLieux().contains(e.getLieux()) && parc.getDate().contains(e.getDate()) && parc.getSecteur().contains(e.getSecteur())){
    			result.put(InputOutput.generateKey(parc.getNom()), parc);
    			System.out.println("nom : "+result.get(result.lastKey()).getNom());
    		}
    	}
    	
    	return result;
    }
    
    ObservableList<String> getAllDate(){
    	ArrayList<String> annees = new ArrayList<String>();
    	
    	for(int i = 2001; i <= 2021; i+=1){
    		String string = String.valueOf(i);
    		string = string.substring(2);
    		int j = Integer.parseInt(string) + 1;
    		string = String.valueOf(j);
    		
    		if(string.length() == 1)
    			string = 0 + string;
    		
    		string = String.valueOf(i)+"-"+string;
    		
    		annees.add(string);
    	}
    	
    	ObservableList<String> result = FXCollections.observableArrayList(annees);
    	return result;
    }
    
    boolean clear(){
    	
    	nomAjouterTextField.setText("");
    	prenomAjouterTextField.setText("");
    	genreAjouterComboBox.getSelectionModel().select(0);
    	etablissementAjouterTextField.setText("");
    	lieuxAjouterTextField.setText("");
    	secteurAjouterComboBox.getSelectionModel().select(0);
    	dateAjouterCombo.getSelectionModel().select(0);
    	
    	return true;
    }
    
    class AnnuleException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = -1797393778821161640L;

		public AnnuleException(String msg){
    		super(msg);
    	}
    }
    
    
    
    class ModifierException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 7760486398053403963L;

		public ModifierException(String msg){
    		super(msg);
    	}
    }
    class SupprimeException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 7760486398053403963L;

		public SupprimeException(String msg){
    		super(msg);
    	}
    }
    
    class SaveException extends Exception{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
		public SaveException(String msg){
			super(msg);
		}
    	
    }
    
    class ExportExcelException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public ExportExcelException(String msg){
    		super(msg);
    	}
    }
    
    class CSVException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public CSVException(String msg){
    		super(msg);
    	}
    }
    class SearchNomException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SearchNomException(String msg){
    		super(msg);
    	}
    }
    class SearchPrenomException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SearchPrenomException(String msg){
    		super(msg);
    	}
    }
    class SearchGenreException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SearchGenreException(String msg){
    		super(msg);
    	}
    }
    class SearchEtablissementException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SearchEtablissementException(String msg){
    		super(msg);
    	}
    }
    class SearchLieuxException extends Exception{
    	/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SearchLieuxException(String msg){
    		super(msg);
    	}
    }
}
