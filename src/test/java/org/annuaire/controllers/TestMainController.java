package org.annuaire.controllers;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import javafx.event.ActionEvent;
import javafx.scene.input.MouseEvent;

public class TestMainController {
	private MainController sujet = new MainController();

	@Test
	public void testCrud() {
		assertTrue(true);
	}

	@Test
	public void testExportFile() throws Exception {
		assertTrue(sujet.exportFile(new ActionEvent()));
	}

	@Test
	public void testImportFile() {
		assertTrue(sujet.importFile(new ActionEvent()));
	}

	@Test
	public void testLeave() {
		assertTrue(true);
	}

	@Test
	public void testGetDesc() {
		assertTrue(sujet.getDesc(new MouseEvent(null, 0, 0, 0, 0, null, 0, false, false, false, false, false, false, false, false, false, false, null)));
	}

	@Test
	public void testInitialize() {
		assertTrue(true);
	}

	@Test
	public void testSearchAll() {
		assertTrue(true);
	}

	@Test
	public void testHandlerTableView() {
		assertTrue(true);
	}

	@Test
	public void testRefreshTab() {
		assertTrue(true);
	}

	@Test
	public void testRefreshTableView() {
		assertTrue(true);
	}

	@Test
	public void testInitCell() {
		assertTrue(true);
	}

	@Test
	public void testAddAllComboBoxValues() {
		assertTrue(true);
	}

	@Test
	public void testShowModifMenu() {
		assertTrue(true);
	}

	@Test
	public void testCleanAllMessages() {
		assertTrue(true);
	}

	@Test
	public void testSearch() {
		assertTrue(true);
	}

	@Test
	public void testGetAllDate() {
		assertTrue(true);
	}

	@Test
	public void testClear() {
		assertTrue(true);
	}

}
